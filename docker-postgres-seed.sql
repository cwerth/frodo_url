CREATE TABLE IF NOT EXISTS url_maps
(
    id character varying,
    original_url character varying,
    PRIMARY KEY (id)
);

-- ALTER TABLE public.url_maps
--     OWNER to postgres;