import React from 'react';
import PropTypes from 'prop-types';


/**
 * React Component for showing errors
 * @param {object} props- component properties.
 * @returns {object} - Component to display the error.
 */
const ErrorComponent = ({error}) => (
    <div className='frodo-error'>
        {console.log('error is', error)}
        {error}
    </div>
);

ErrorComponent.propTypes = {
    /**error to display to the user */
    error: PropTypes.string.isRequired,
};

export default ErrorComponent;