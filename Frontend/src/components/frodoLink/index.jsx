import React from 'react';
import PropTypes from 'prop-types';

/**
 * React Component for returning frodo link
 * @param {object} props- component properties.
 * @returns {object} - Component to display the shortUrl.
 */
const FrodoLink = ({shortUrl}) => (
    <div className='frodo-link'>
        <label>your short url is:</label>
        <h2>{shortUrl}</h2>
    </div>
);

FrodoLink.propTypes = {
    /**shortUrl to display to the user */
    shortUrl: PropTypes.string.isRequired,
};

export default FrodoLink;