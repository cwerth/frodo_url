import './home.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import ErrorComponent from '../error';
import FrodoLink from '../frodoLink';

// import ReactDOM from 'react-dom';
const backend_uri = `http://${process.env.REACT_APP_BACK_END_HOST}:${process.env.REACT_APP_BACK_END_PORT}`
const frontend_uri = `http://localhost:3000`;
function Home() {
  const [url, setUrl] = useState("");
  const [error, setError] = useState("");
  const [shortUrl, setShortUrl] = useState("");

  useEffect(() => {
    const queryParams = new URLSearchParams(window.location.search);

    const id = queryParams.get('id');
    if (id != null){

      axios.get(`${backend_uri}/frodo`, {params: {id: id}}).then((response => {
        var result = response.data;
        if (result.error) {
          setError("No redirect url found, please check your id")
        }
        else {
          window.location.href=(result.url)

        }
      }))
    } else {
      
    }
  }, [])

  const confirmUrl = (url) => {
    setUrl(url)
  }

  const submitUrl = (event) => {
    event.preventDefault();
    if (!url) {
      setError("Please enter a url");
      setShortUrl("");
      return
    }
    else {
      setError("")
      axios.post(`${backend_uri}/make-short`, {originalUrl: url}).then((response => {
      var result = response.data;
      setShortUrl(`${frontend_uri}/?id=${result.pk}`)
    }));
  }
}
  return (
<>
    <form className='frodo-form' onSubmit={submitUrl}>
      <h1 className='frodo-header'>Enter a url in the field below to recieve a short url</h1>
        <input
          className='frodo-input'
          type="text"
          value={url}
          onChange={(e) => confirmUrl(e.target.value)}
        />
      <input type="submit"/>
    </form>

    {shortUrl !== "" && (
      <FrodoLink shortUrl={shortUrl}/>
    )}

    {error !== "" && (
      <ErrorComponent error={error}/>
    )}
</>
  );
}

export default Home;
