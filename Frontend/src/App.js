import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './components/home/index';
import About from './components/about/index';

function App() {
  return (
    <div>
      <Router>
        <Routes>
          <Route path="/" exact element={<Home/>} />
          <Route path="/about" element={<About/>}/>
        </Routes>
      </Router>
    </div>
  );
}

export default App