# frodo_url

app for creating short urls

## Getting started

to run this app, you will need to download postgres. the latest version can be found here: https://www.postgresql.org/download/

go through the initial setup, keeping the default user and port. also install pgAdmin. Note any password you set up, as you'll need that in the config later.

Once postgres is finished installing, open pgAdmin and run the command below to setup the database table needed for the app.

CREATE TABLE IF NOT EXISTS public.url_maps
(
    id character varying,
    original_url character varying,
    PRIMARY KEY (id)
);

ALTER TABLE public.url_maps
    OWNER to postgres;

to get the app started, navigate to the backend folder and run the following command to install necessary dependincies:
pip install -r requirements.txt

next, open the file variables.py and update it with your postgres db settings.
After everything is filled out, open a terminal and run the following command to start the backend:
python app.py

Next, open a terminal and navigate the Frontend folder. Run the following command:
npm install.

Once the install is complete, run the following command to start the front end:
npm start

A webpage should open, and you can now put in a url of your choosing and you will receive a short url that will redirect you to your original url.

## Next Steps
First, I'd clean up the app. separate the css into separate files for each component.
I'd move from using straight sql queries into using sql Alchemy. I would also set up/seed the databse using alembic. Moving forward, alembic migrations would be how we handle upgrading the db. I'd also implement using docker containers. Once we have that set up, starting a local stack that includes a docker db that is already seeded would be easy and great for local development.
I would also add some better checks for the url. Best bet would be to just quickly hit the site and confirm we get a 200 from the request before creating a short url.

As for user experience, it might be nice to implement a kind of favorites and/or recent links they created. Given these links may be to useful trainings or confluence pages, it would also be nice to implement a tag or description block that could be tied to the url. Then when a user goes to the page they would already have their favorite or recent short links, along with a quick description that helps them find the one they want and then send it off.

Testing - We would implement jest tests for the backend. Frontend I would like to move forward using cypress. It works well and gives a gui interface which can be nice for running front end tests.

10/4/23
updated frontend to use routing
got the docker-compose up for the db. started a dockerfile for the backend. I can build the file, but when I try to run it can't connect to the db (docker network stuff). solution - put it in the docker-compose

10/22/23
made docker file for frontend and backend. 
made docker-compose to spin up apps.
modified python app.py file to use 0.0.0.0 instead of localhost.