import flask
from flask import Response, request
from flask_cors import CORS, cross_origin 
import psycopg2
from uuid import uuid4
import os

app = flask.Flask(__name__)
CORS(app, resources={r"/*": {"origins": "http://localhost:3000"}})
app.config["DEBUG"] = True
print('LINE 11')
dbsettings = {'database': os.environ.get('DB_NAME'),
            'user': os.environ.get('DB_USER'),
            'password': os.environ.get('DB_PASSWORS'),
            'host': os.environ.get('DB_HOST')}

my_conn = psycopg2.connect(**dbsettings)
pg_curr = my_conn.cursor()

def check_url(url: str) -> str:
    """_summary_
    Args:
        url (str): url to check if exists in our db
    Returns:
        str: url id if it exists
    """
    originalUrl = url
    if not originalUrl.startswith('http'):
        originalUrl = 'http://'+originalUrl
    pg_curr.execute('SELECT id FROM url_maps where original_url = %s', (originalUrl,))
    return pg_curr.fetchone()

def create_url(url: str) -> str:
    url_id = str(uuid4())[:8]
    pg_curr.execute('INSERT INTO url_maps (id, original_url) VALUES (%s, %s)', (url_id, url))
    my_conn.commit()
    return url_id

def fetch_original(id: str) -> str:
    pg_curr.execute('SELECT original_url FROM url_maps where id = %s', (id,))
    # try to fetch a redirect url from the short url.
    return pg_curr.fetchone()

@app.route('/make-short', methods=["POST"])
def make_short () -> Response:
    """return a short url from original url"""
    # first set url entered to lower and add http before it if it doesn't already have
    # it. Then check the db to see if a short url already exists for the url entered
    formaturl = request.json['originalUrl'].lower()
    if not formaturl.startswith('http'):
        formaturl = 'http://'+formaturl
    url_id = check_url(formaturl)
    # if the short url exists, return it to the user, otherwise create it
    return flask.jsonify({"pk": url_id if url_id else create_url(formaturl)}) 


@app.route('/frodo', methods=["GET"])
def frodo_route () -> Response:
    """Gets original url and redirects user"""
    request_id = request.args.get('id')
    redirect_url = fetch_original(request_id)
    return flask.jsonify({'url': redirect_url[0]}) if redirect_url else flask.jsonify({'error': 'error'})

@app.route('/test', methods=["GET"])
def test () -> Response:
    return flask.jsonify({'test': 'complete'})

if __name__ == '__main__':
    print('APP STARTED ')
    app.run(host='0.0.0.0', port="5000")